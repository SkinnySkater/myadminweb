<?php
session_start();
require_once 'include/DB_Functions.php';
$db = new DB_Functions();

$ischeck = False;
$isgood = False;

$isupdate = False;
$isgoodup = False;

if (!isset($_SESSION["myusername"]))
{
	header("location:main_login.php");
}

if (isset($_GET['checking']) and $_GET['checking'] == "True")
 {
	$ischeck = True;
	$title = $_POST['title'];
	$description = $_POST['description'];
	$full_text = $_POST['text'];
	$isgood = $db->check_add($title, $description, $full_text);
	if ($isgood)
		$db->add_article($title, $description, $full_text);
 }

if (isset($_GET['update']) and $_GET['update'] == "True" and isset($_GET['id']))
 {
	$isupdate = True;
	$id = $_GET['id'];
	$_GET['id'] = $id;
	$article = $db->get_art_by_id_up($id);
 }

 if (isset($_GET['del']))
 {
 	$id = $_GET['del'];
 	$db->del_article($id);
 }

if (isset($_GET['up']) and isset($_POST['$title']) and isset($_GET['id']))
{
	$id = $_GET['id'];
	$db->update_article($id, $_POST['$title'], $_POST['$description'], $_POST['$full_text']);
}

?>
 <head>
 	<link rel="stylesheet" type="text/css" href="assets/css/style2.css">
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">	
</head> 
<html>

	<body class="bd">
		<section class="short">
			<?php
				$name =$_SESSION["myusername"];
				echo "<div class=\"wrapper\"><h1 class=\"alpha lato thick-header-line uppercase ls-small mt \">Welcome $name !</h1>\n";
			?>
			<br>
			<a href="index.html"><button class="btn btn-lg">Website</button></a>
			</div>
		</section>
		<br>
		<section class="mt">
			<?php
				if ($ischeck and $isgood)
					{
						echo "<h1 class=\"alpha lato thick-header-line green_line uppercase ls-small midle_title txtsuccess\">Add Successfull!</h1>\n";
					}
					else
					{
						echo "<h1 class=\"alpha lato thick-header-line uppercase ls-small midle_title\">Add New Article</h1>\n";
					}
			?>
			<?php
				if (!$ischeck or ($ischeck and $isgood))
				{
					?>
					<div class="wrapper">
						<form class="pure-form" action="login_success.php?checking=True" method="post">
						    <div class="pure-group">
						        <input type="text" class="pure-input-1-2" placeholder="Title" name="title" id="title">
						        <input type="text" class="pure-input-1-2" placeholder="Description" name="description" id="description">
						    </div>

						    <div class="pure-group">
						        <textarea class="pure-input-1-2" placeholder="Full Article" name="text" id="text"></textarea>
						    </div>
						    <button type="submit" name="Submit" class="pure-button pure-input-1-2 pure-button-primary">Add Article!</button>
					    </form>
				    </div>
			    <?php
			}
			else
			{
				 if ($ischeck and !$isgood)
				{
					echo "<div class=\"wrapper\">\n";
					echo "<h2 class=\"txtwrong \">Please Complete all the input before submitting!</h2>\n";
	    			echo "<form class=\"pure-form\" action=\"login_success.php?checking=True\" method=\"post\">\n";
					echo "<div class=\"pure-group\">\n";
	    			if (empty($title))
			        	echo "<input class=\"input_wrong\" type=\"text\" class=\"pure-input-1-2\" placeholder=\"Title\" name=\"title\"id=\"title\">\n";
			        else
			        	echo "<input type=\"text\" class=\"pure-input-1-2\" placeholder=\"Title\" name=\"title\" id=\"title\" value=\"$title\">\n";

			        if (empty($description))
			        	echo "<input class=\"input_wrong\" type=\"text\" class=\"pure-input-1-2\" placeholder=\"Description\" name=\"description\" id=\"description\">\n";
			        else
			        	echo "<input type=\"text\" class=\"pure-input-1-2\" placeholder=\"Description\" name=\"description\" id=\"description\" value=\"$description\">\n";
			        echo "</div>\n";

			        echo "<div class=\"pure-group\">\n";
				    if (empty($full_text))
				        echo "<textarea class=\"pure-input-1-2 textarea_wrong\" placeholder=\"Full Article\" name=\"text\" id=\"text\"></textarea>\n";
				    else
				    	echo "<textarea class=\"pure-input-1-2\" placeholder=\"Full Article\" name=\"text\" id=\"text\">$full_text</textarea>\n";
				    echo "</div>\n";
				    echo "<button type=\"submit\" class=\"pure-button pure-input-1-2 pure-button-primary\">Add Article!</button>\n";
				    echo "</form>\n";
				    echo "</div>\n";
				}
			}
			?>
		</section>
		
		<br>

		<section>
			<h1 class="alpha lato thick-header-line  uppercase ls-small midle_title ">Delete Article</h1>
			<div class="wrapper_art padleftwrap">
					<div class="left-half">
					    <article class="article">
					    <h1 class="gamma light merriweather italic text-left mt">Scroll Down to <span class="color-emphasis-1">Delete Article</span></h1>
					    <?php if (isset($_GET['del']))
							{
								echo "<h1 class=\"gamma light merriweather italic text-left mt\">Article number: $id is Deleted!</h1>";
							}
			 			?>
					    </article>
					 </div>
					<div class="right-half-scroller ">
					    <article class="article">
					     		<ul class="bargraph">
								    <?php $db->gen_del_articles() ?>
								</ul>
					    </article>
					</div>
				</div>
		</section>
		<br>
		<section>
			<h1 class="alpha lato thick-header-line  uppercase ls-small midle_title ">Update Article</h1>
			<div class="wrapper_art ">
					<div class="left-half">
					    <article class="article">
					    <form class="pure-form" action="login_success.php?up=True?id=$id" method="post">
						    <div class="pure-group">
							    <?php 
							    	if (!$isupdate)
									{
										echo "<input type=\"text\" class=\"pure-input-1-2\" placeholder=\"Title\" name=\"title\" id=\"title\">\n";
							       	 	echo "<input type=\"text\" class=\"pure-input-1-2\" placeholder=\"Description\" name=\"description\" id=\"description\">\n";
									}
									else
									{
										$t = $article['title'];
										$d = $article['description'];
										echo "$t";
										echo "<input type=\"text\" class=\"pure-input-1-2\" placeholder=\"Title\" name=\"title\" id=\"title\" value=\"$t\">\n";
							        	echo "<input type=\"text\" class=\"pure-input-1-2\" placeholder=\"Description\" name=\"description\" id=\"description\" value=\"$d\">\n";
									}
					 			?>
						    </div>
						    <div class="pure-group">
						    <?php 
							    	if (!$isupdate)
									{
										echo "<textarea class=\"pure-input-1-2\" placeholder=\"Full Article\" name=\"text\" id=\"text\"></textarea>\n";
									}
									else
									{
										echo "<textarea class=\"pure-input-1-2\" placeholder=\"Full Article\" name=\"text\" id=\"text\">{$article['full_text']}</textarea>";
									}
					 			?>
						    </div>
						    <button type="submit" name="Submit" class="pure-button pure-input-1-2 pure-button-primary">Update Article!</button>
					    </form>
					    <?php if (isset($_GET['up']))
							{
								echo "<h1 class=\"gamma light merriweather italic text-left mt\">Article number: $id is Updated!</h1>";
							}
			 			?>
					    </article>
					 </div>
					<div class="right-half-scroller ">
						<br>
					    <article class="article">
					     		<ul class="bargraph">
								    <?php $db->gen_up_articles() ?>
								</ul>
					    </article>
					</div>
				</div>
		</section>

	</body>
</html>