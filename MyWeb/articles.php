 <?php
  session_start();
  require_once 'include/DB_Functions.php';
  $db = new DB_Functions();

  if (isset($_GET['range']))
  {
  	$offset = $_GET['range'];
  }
?>
 <!DOCTYPE html>
 <html>
 <head>
 	<link rel="stylesheet" type="text/css" href="assets/css/style2.css">
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">	
</head> 
	<body>
		 <section class="sectionf">
			<div class="wrapper">
				<h1 class="beta uppercase montserrat regular line-after-heading">
					Ovila Julien Lugard
				</h1>
				<p class="delta cardo regular italic">
					Creative Developer, Science Lover, Hack Enthusiast & Adventurer
				</p>

				<img src="assets/img/photo.jpeg" class="img-circle-profil" alt="Paris">

				<br>
				<h1 class="gamma regular merriweather italic text-left">Looking for Admin Sys/Software Dev Alternance</h1>

				<br>
				<h4 class="delta oswald regular uppercase ls-large">Follow Me !</h4>

				<div class="social">
					  <a href="https://www.facebook.com/kawashi.kanmuri" class="link facebook" target="_parent"><span class="fa fa-facebook"></span></a>
					  <a href="https://twitter.com/Julien_Lu?lang=fr" class="link twitter" target="_parent"><span class="fa fa-twitter"></span></a>
					  <a href="https://www.linkedin.com/in/ovila-julien-lugard-239324b1" class="link linkedin" target="_parent"><span class="fa fa-linkedin"></span></a>
					  <a href="https://bitbucket.org/SkinnySkater" class="link github" target="_parent"><span class="fa fa-bitbucket"></span></a>
				</div>

			 </div>
		 </section>
	
		<section class="short">
			<div class="wrapper"><h1 class="alpha lato thin thick-header-line uppercase ls-small mt">My Articles</h1>
			<br>
			<a href="index.html"><button class="btn btn-lg">Home</button></a>
			</div>
		</section>
		
        <?php  $db->gen_articles($offset) ?>

		<section class="wrapper_art short">
			<?php if ($offset > 0)
					{
						$offset -= 5;
						echo "<a class=\"page\" href=\"articles.php?range=". $offset . "\"><div class=\" fa fa-chevron-left icone\"></div></a>\n";
					}
					$tmp = $offset + 5;
	        		$count = $db->get_row($tmp);
	        		if ($count > 0)
					{
						$offset += 5;
						echo "<a class=\" page\" href=\"articles.php?range=". $offset . "\"><div class=\" fa fa-chevron-right icone\"></div></a>";
					}
			?>
		</section>
	
</body>
</html>