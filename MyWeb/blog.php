<?php
  session_start();
  require_once 'include/DB_Functions.php';
  $db = new DB_Functions();

  if (isset($_GET['id']))
  {
    $id = $_GET['id'];
    $res = $db->get_art_by_id($id);
  }
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog Post - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/blog-post.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <p class="navbar-brand dark_blue">Ovila Julien Lugard</p>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active bottom">
              <a class="nav-link btn btn-lg" href="index.html"><button class="btn btn-lg">Home</button>
              </a>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <div class="row">      

        <!-- Post Content Column -->
        <div class="col-md-12">

          <!-- Title -->
          <h1 class="mt-4"><?php echo $res["title"]; ?></h1>

          <!-- Date/Time -->
          <p><?php echo "Posted on , "; echo $res["create_date"]; ?></p>

          <hr>

          <!-- Preview Image -->
          <img class="img-fluid rounded center_img" src="http://placehold.it/900x300" alt="">

          <hr>

          <!-- Post Content -->
          <p class="lead"><?php echo $res['description']; ?></p>

          <p><?php echo str_replace("\n", "<br><br>", $res['description']); ?></p>

          <hr>

    </div>
  </div>
      <!-- /.row -->
  </div>
<!-- /.container -->

  </body>

</html>
