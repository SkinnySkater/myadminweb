<?php

class DB_Functions
{
    private $conn;

    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }

    // destructor
    function __destruct() {}

    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($name, $email, $password)
    {
        $uuid = uniqid('', true);
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt

        $stmt = $this->conn->prepare("INSERT INTO users(unique_id, name, email, encrypted_password, salt, created_at) VALUES(?, ?, ?, ?, ?, NOW())");
        $stmt->bind_param("sssss", $uuid, $name, $email, $encrypted_password, $salt);
        $result = $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            return $user;
        } else {
            return false;
        }
    }

    public function gen_articles($offset)
    {
        $stmt = $this->conn->prepare("SELECT id, title, create_date, description FROM articles ORDER BY articles.create_date DESC LIMIT 5 OFFSET $offset");

        if ($stmt->execute())
        {
            $stmt->bind_result($id, $title, $create_date, $description);

            /* Lecture des valeurs */
            while ($stmt->fetch())
            {
                echo "<section>\n";
                echo "  <div class=\"wrapper_art\">\n";
                echo "      <div class=\"right-half dark_background\"\n>";
                echo "          <article class=\"mt\">\n";
                echo "              <h2 class=\"gamma light merriweather italic \">"; echo $description . "</h2>\n";
                echo "                  <br>\n";
                echo "                  <a href=\"" . "action.php?id=" . $id . "\"><button class=\"btn btn-lg\">Open article</button></a>\n";
                echo "               </article>\n";
                echo "             </div>\n";
                echo "            <div class=\"left-half dark_background\">\n";
                echo "                <article class=\"mt\">\n";
                echo "                        <h1 class=\"decorative-span delta oswald ls-xlarge regular uppercase\">" . $title . "</h1>\n";
                echo "                        <br>\n";
                echo "                        <h3 class=\"delta thin raleway color-emphasis-2 ls-medium h3\">" . $create_date . "</h3>\n";
                echo "                </article>\n";
                echo "            </div>\n";
                echo "        </div>\n";
                echo "    </section>\n\n";
            }
            $stmt->close();
        }
    }

    public function gen_del_articles()
    {
        $stmt = $this->conn->prepare("SELECT id, title, create_date FROM articles ORDER BY articles.create_date DESC");

        if ($stmt->execute())
        {
            $stmt->bind_result($id, $title, $create_date);

            #$user = $stmt->get_result()->fetch_assoc();

            /* Lecture des valeurs */
            while ($stmt->fetch())
            {
                echo "<a href=\"login_success.php?del=$id\"><li class=\"space delta oswald regular blackchart\">$title<span class=\"floatright\">$create_date</span></li></a>\n";
            }
            $stmt->close();
        }
    }

    public function gen_up_articles()
    {
        $stmt = $this->conn->prepare("SELECT id, title, create_date FROM articles ORDER BY articles.create_date DESC");

        if ($stmt->execute())
        {
            $stmt->bind_result($id, $title, $create_date);

            /* Lecture des valeurs */
            while ($stmt->fetch())
            {
                echo "<a href=\"login_success.php?update=True?id=$id\"><li class=\"space delta oswald regular blackchart\">$title<span class=\"floatright\">$create_date</span></li></a>\n";
            }
            $stmt->close();
        }
    }

    public function get_row($offset)
    {
        $stmt = $this->conn->prepare("SELECT * FROM articles ORDER BY articles.create_date DESC LIMIT 5 OFFSET $offset");
        $row = 0;

        if ($stmt->execute())
        {
            $stmt->store_result();

            $row = $stmt->num_rows;

            $stmt->close();
            return $row;
        }
    }

    public function get_art_by_id($id)
    {
        $res = array();
        $stmt = $this->conn->prepare("SELECT title, create_date, description, full_text FROM articles WHERE articles.id = $id");

        if ($stmt->execute())
        {
            $stmt->bind_result($title, $create_date, $description, $full_text);

            /* Lecture des valeurs */
            if ($stmt->fetch())
            {
                $res = ['title' => $title, 'create_date' => $create_date, 'description' => $description, 'full_text' => $full_text];
            }
            $stmt->close();
        }
        return $res;
    }

    public function get_art_by_id_up($id)
    {
        $res = array();
        $stmt = $this->conn->prepare("SELECT id, title, description, full_text FROM articles WHERE articles.id = $id");

        if ($stmt->execute())
        {
            $stmt->bind_result($id, $title, $description, $full_text);

            /* Lecture des valeurs */
            if ($stmt->fetch())
            {
                $res = ['title' => $title, 'id' => $id, 'description' => $description, 'full_text' => $full_text];
            }
            $stmt->close();
        }
        return $res;
    }

    public function del_article($id)
    {
        $sql = "DELETE FROM articles WHERE id = $id";

        $stmt = $this->conn->prepare($sql);

        $stmt->execute();

        $stmt->close();
    }

    public function update_article($id, $title, $description, $full_text)
    {
        $sql = "UPDATE articles";
        $sql .= " SET title = '$title',";
        $sql .= " SET description = '$description',";
        $sql .= " SET full_text = '$full_text'";
        $sql .= " WHERE id = $id";

        $stmt = $this->conn->prepare($sql);

        $stmt->execute();

        $stmt->close();
    }


    public function check_add($title, $description, $full_text)
    {
        return (!empty($title) and !empty($description) and !empty($full_text));
    }

    public function add_article($title, $description, $full_text)
    {
        $sql = "INSERT INTO articles VALUES (DEFAULT, '$title', DEFAULT, '$description', '$full_text')";

        $stmt = $this->conn->prepare($sql);

        $stmt->execute();

        $stmt->store_result();

        $stmt->close();

        //header("Refresh:0");
    }


    public function isUserExisted($user, $pwd)
    {
        $stmt = $this->conn->prepare("SELECT username from user WHERE username = ? AND password = ?");

        $stmt->bind_param("ss", $user, $pwd);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        }
        else
        {
            // user not existed
            $stmt->close();
            return false;
        }
    }


    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */
    public function hashSSHA($password)
    {
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

    /**
     * Decrypting password
     * @param salt, password
     * returns hash string
     */
    public function checkhashSSHA($salt, $password) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }

}

?>
